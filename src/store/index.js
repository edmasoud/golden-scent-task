import axios from 'axios';
import { createStore } from 'vuex';

export default createStore({
  state: {
    allProducts: [],

  },
  mutations: {
    setAllProducts(state,payload) {
      state.allProducts = payload;
    }
  },
  actions: {
    async getProductsList(state) {
      let productsList = await axios.get("https://gs-euw1-public-data-prod.s3-eu-west-1.amazonaws.com/new-web/test/products.json");
      productsList = productsList.data.hits;
      console.log(productsList);
      state.commit("setAllProducts",productsList);
    }
  },
  modules: {},
  getters: {
    getAllProducts: state => state.allProducts,
  },
});
